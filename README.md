# Cat app

Test assignment.<br />
This project has been tested on Android, iOS, Web.

## Getting Started

First, in the root directory you need to create ```.env``` file, where you should put your API_KEY for Cats API.<br />
Example:
```
API_KEY="YOUR_API_KEY"
```
Without this, you can't launch the app.

After that run the app by command:
```
flutter run
```

NOTE: To run the app in Chrome, use the following command:
```
flutter run -d chrome --web-renderer html
```
There is some problem with loading network images in Chrome browser, so this is the temporary soltuion of it.

## Demo

![](demo.mp4)<br />
![](demo.jpg)

