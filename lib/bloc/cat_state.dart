import 'package:equatable/equatable.dart';

abstract class CatState extends Equatable {
  const CatState();

  @override
  List<Object?> get props => [];
}

class DataLoadingState extends CatState {}

class DataErrorState extends CatState {}

class DataLoadedState extends CatState {
  final List data;
  const DataLoadedState({this.data = const []});

  @override
  List<Object> get props => [data];
}
