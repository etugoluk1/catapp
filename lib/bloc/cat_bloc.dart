import 'package:bloc/bloc.dart';
import 'package:cat_app/bloc/cat_event.dart';
import 'package:cat_app/bloc/cat_state.dart';
import 'package:cat_app/repositories/cats_repository.dart';

class CatBloc extends Bloc<CatEvent, CatState> {
  final CatsRepository _catsRepository;

  CatBloc({required CatsRepository catsRepository})
      : _catsRepository = catsRepository,
        super(DataLoadingState()) {
    on<LoadBreedDataEvent>((event, emit) async {
      emit(DataLoadingState());
      final data = await _catsRepository.getBreedsList();
      if (data.isEmpty) {
        emit(DataErrorState());
      } else {
        emit(DataLoadedState(data: data));
      }
    });
  }
}
