import 'dart:async';

import 'package:cat_app/models/breed.dart';
import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class CatsRepository {
  final String? _apiKey;
  CatsRepository() : _apiKey = dotenv.env['API_KEY'];

  Future<void> setBreedImage(Breed item) async {
    final response = await Dio()
        .get('https://api.thecatapi.com/v1/images/search?breed_id=${item.id}');
    item.imageUrl = response.data[0]['url'];
  }

  Future<List> getBreedsList() async {
    try {
      if (_apiKey == null) {
        throw Exception(
            'API KEY is not available. Make sure you set it in .env file');
      }
      final response = await Dio()
          .get('https://api.thecatapi.com/v1/breeds?api_key=$_apiKey');

      final List breedsList =
          response.data.map((item) => Breed.fromJson(item)).toList();

      // we need to load images which user will see first
      for (var i = 0; i < 4; i++) {
        await setBreedImage(breedsList[i]);
      }

      Stream.fromIterable(breedsList.sublist(4))
          .asyncMap((item) => setBreedImage(item))
          .toList();

      return breedsList;
    } catch (e) {
      print(e);
      return [];
    }
  }
}
