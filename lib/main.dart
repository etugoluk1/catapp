import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:cat_app/bloc/cat_bloc.dart';
import 'package:cat_app/bloc/cat_event.dart';
import 'package:cat_app/repositories/cats_repository.dart';
import 'package:cat_app/screens/main_screen.dart';
import 'package:cat_app/theme/theme_constants.dart';

void main() async {
  await dotenv.load(fileName: ".env");
  runApp(const CatApp());
}

class CatApp extends StatelessWidget {
  const CatApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: theme,
      home: BlocProvider(
        create: (context) => CatBloc(
          catsRepository: CatsRepository(),
        )..add(LoadBreedDataEvent()),
        child: const MainScreen(),
      ),
    );
  }
}
