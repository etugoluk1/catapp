import 'package:flutter/material.dart';

ThemeData theme = ThemeData(
  brightness: Brightness.light,
  scaffoldBackgroundColor: Colors.grey[50],
  primaryColor: Colors.white,
  errorColor: const Color(0xffea4c89),
  appBarTheme: AppBarTheme(
    foregroundColor: Colors.black87,
    backgroundColor: Colors.grey[50],
    centerTitle: true,
    elevation: 0,
    iconTheme: const IconThemeData(color: Colors.grey),
  ),
  colorScheme: ColorScheme.fromSeed(
    seedColor: Colors.black12,
    primary: Colors.grey,
  ),
);
