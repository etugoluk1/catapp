import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:cat_app/models/breed.dart';

class BreedItem extends StatelessWidget {
  final Breed item;
  final bool isFavorite;
  final Function setFavoriteCallback;

  const BreedItem(this.item, this.isFavorite, this.setFavoriteCallback,
      {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      elevation: 4,
      margin: const EdgeInsets.all(12),
      child: Column(
        children: [
          ClipRRect(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15),
            ),
            child: Stack(alignment: AlignmentDirectional.bottomEnd, children: [
              item.imageUrl != null
                  ? Image.network(
                      item.imageUrl!,
                      height: 170,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    )
                  : Image.asset(
                      'assets/no_cat_image.png',
                      height: 170,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
              IconButton(
                icon: isFavorite
                    ? const Icon(
                        Icons.favorite,
                        size: 26,
                        color: Color.fromRGBO(255, 102, 146, 1),
                      )
                    : const Icon(
                        Icons.favorite_border,
                        size: 26,
                        color: Colors.white,
                      ),
                onPressed: () {
                  setFavoriteCallback(item.id, !isFavorite);
                },
              ),
            ]),
          ),
          Padding(
            padding: const EdgeInsets.all(12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  item.name,
                  textAlign: TextAlign.start,
                  style: const TextStyle(fontSize: 26),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        const Icon(Icons.location_on_outlined),
                        Text(item.origin),
                      ],
                    ),
                    if (item.wikipediaUrl != null)
                      InkWell(
                          child: const Text(
                            "Wiki",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          onTap: () => launchUrl(Uri.parse(item.wikipediaUrl!))),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
