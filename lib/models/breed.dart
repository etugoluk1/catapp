import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'breed.g.dart';

@JsonSerializable()
class Breed extends Equatable {
  final String id;
  final String name;
  final String origin;
  @JsonKey(name:'wikipedia_url')
  final String? wikipediaUrl;
  @JsonKey(ignore: true)
  String? imageUrl;

  Breed({
    required this.id,
    required this.name,
    required this.origin,
    required this.wikipediaUrl,
    this.imageUrl,
  });

  factory Breed.fromJson(Map<String, dynamic> json) => _$BreedFromJson(json);

  Map<String, dynamic> toJson() => _$BreedToJson(this);

  // static Breed fromJson(Map<String, dynamic> json) {
  //   return Breed(
  //     id: json['id'],
  //     name: json['name'],
  //     origin: json['origin'],
  //     wikiUrl: json['wikipedia_url'],
  //   );
  // }

  @override
  List<Object> get props => [name, origin];
}
