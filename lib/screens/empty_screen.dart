import 'package:flutter/material.dart';

class EmptyScreen extends StatelessWidget {
  final String msg;
  final TextStyle? style;
  const EmptyScreen(this.msg, {this.style, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Image.asset(
            'assets/empty_screen.png',
            height: MediaQuery.of(context).size.height * 0.4,
          ),
          const SizedBox(height: 30),
          Text(
            msg,
            textAlign: TextAlign.center,
            style: style ?? const TextStyle(fontWeight: FontWeight.w400),
          ),
          const SizedBox(height: 50),
        ],
      ),
    );
  }
}
