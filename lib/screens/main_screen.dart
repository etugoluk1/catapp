import 'package:cat_app/screens/empty_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:cat_app/bloc/cat_bloc.dart';
import 'package:cat_app/bloc/cat_state.dart';
import 'package:cat_app/widgets/breed_item.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  late SharedPreferences prefs;
  bool showOnlyFavourites = false;

  @override
  void initState() {
    super.initState();
    _loadSharedPreferences();
  }

  void _loadSharedPreferences() async {
    prefs = await SharedPreferences.getInstance();
  }

  void _setFavoriteValue(String id, bool value) async {
    await prefs.setBool(id, value);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'CatApp',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        actions: [
          PopupMenuButton<bool>(
            offset: const Offset(0, 50),
            itemBuilder: (context) => const [
              PopupMenuItem<bool>(
                value: true,
                child: Text('Show favorites'),
              ),
              PopupMenuItem<bool>(
                value: false,
                child: Text('Show all'),
              ),
            ],
            onSelected: (value) {
              setState(() {
                showOnlyFavourites = value;
              });
            },
            icon: const Icon(Icons.menu),
          ),
        ],
      ),
      body: BlocBuilder<CatBloc, CatState>(builder: (context, state) {
        if (state is DataLoadedState) {
          var data = showOnlyFavourites
              ? state.data
                  .where((element) => prefs.getBool(element.id) == true)
                  .toList()
              : state.data;
          if (data.isEmpty) {
            return EmptyScreen(
              'No cats here...',
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey[600]),
            );
          } else {
            return Container(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: ListView.builder(
                itemBuilder: ((context, index) {
                  var isFav = prefs.getBool(data[index].id) ?? false;
                  return BreedItem(data[index], isFav, _setFavoriteValue);
                }),
                itemCount: data.length,
              ),
            );
          }
        } else if (state is DataLoadingState) {
          return const Center(child: CircularProgressIndicator());
        } else if (state is DataErrorState) {
          return const EmptyScreen(
              'Meow!\nAn error occured during data loading.\nCheck your internet connection or try again later.');
        } else {
          return const EmptyScreen(
              'Meow!\nInternal error. Contact the developers or try again later.');
        }
      }),
    );
  }
}
